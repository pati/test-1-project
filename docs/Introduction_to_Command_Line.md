# Introduction to Command Line

## Learning objective
* Introduce fundamentals of the Linux Command-line to navigate a file system

## Learning outcomes
* Identify a linux-based command-line's interface, potentials and limitations.
* Use basic shell commands to visulize files and folders within a file system.
* Use commands to visualize and edit text files.
* Demonstrate chaining of multiple shell commands



https://docs.google.com/presentation/d/1XRm45_zSJw7-2FAFOb_vxJq00BgBa2TxwNZtJ3zuWg4/edit#slide=id.g13b4e02a5db_0_0

learning outcomes "what you actually get"
learning objectives "what the session is trying to achieve"
“Learner profiles”